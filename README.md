# EveryDay

A **Journal** app where you can write down your day-to-day activities and share them with your friends.<br/>
This is just a demo app that I developed to improve my designing & coding skills and also firebase knowledge.<br/>
Developed using **VIPER** architeture.
I've used **Firebase** as a backend for handling Database and stuff.

The template in the app that I used is of [Rejo Varghese](https://www.uplabs.com/rejov) and downloaded from [uplabs](https://www.uplabs.com/posts/everyday-journal-app).
*Thank you very much* Rejo for publishing this great UI template for free.
